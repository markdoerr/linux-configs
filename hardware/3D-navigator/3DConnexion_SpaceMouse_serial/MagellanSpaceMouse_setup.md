# 3D Connexion Magellan SpaceMouse setup

## Hardware connection

serial SpaceMouse connected to USB Adapter

## Installation

required packages:

	apt install spacenavd libspnav-dev

dmesg

## Configuration 

Create a file named  /etc/spnavrc

	sudo micro /etc/spnavrc

Add 

----------- /etc/spnavrc ------

# file: /etc/spnavrc
# Serial device
# Set this only if you have a serial device, and make sure you specify the
# correct device file. If you do set this option, any USB devices will be
# ignored!
serial = /dev/ttyUSB3  # check correct port with dmesg

-----------

Restarting spacenavd:

	sudo service spacenavd status
	sudo service spacenavd stop
	sudo service spacenavd start

A beeping sound should be hearable.

## Configuration in Blender

Got to edit->Preferences->input->NDOF

  adjust sensitivity 
  regulate inversion
  
