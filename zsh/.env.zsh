# further zsh configurations

# keeping history of all open shells

setopt share_history

# adding more aliases


if [ -f $HOME/.bash_aliases ]; then
    . $HOME/.bash_aliases
fi

if [ -f $HOME/.bash_functions ]; then
    . $HOME/.bash_functions
fi

# save history of a certain profile to a new file:
if [[ -n $TERMPROFILE ]]; then
    HISTFILE=$HOME/.zsh_history.$TERMPROFILE
fi
