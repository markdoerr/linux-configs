
# zsh configuration

## Installation

```bash
apt install zsh
zsh
# this will run the zsh-newuser-install command for setting up the zsh
```

The `zsh-newuser-install` command is run the first time when zsh is started (on Linux Mint).
This generates a default configuration. If you run *zsh4humans*, this config is replaced.

## [zsh4humans](https://github.com/romkatv/zsh4humans)

To have a very nice configuration of the zsh execute the zsh4humans script 
from [zsh4humans](https://github.com/romkatv/zsh4humans) - this will also set zsh as default login shell:


```bash
if command -v curl >/dev/null 2>&1; then
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/romkatv/zsh4humans/v5/install)"
else
  sh -c "$(wget -O- https://raw.githubusercontent.com/romkatv/zsh4humans/v5/install)"
fi
```

This will start a zsh configuration which will be start up zsh much faster than with 
oh-my-zsh !


## Set aliases

1. Copy the bash/.bash_aliases in the $HOME directory
2. add `zsh source ~/.bash_aliases` to the `$HOME/.zshrc` file

(you need to re-source or re-login that changes take effect)

## make zsh the default shell

**not required, when *zsh4humans* is installed**

This command sets the *zsh* as default shell for the current user:

`chsh -s $(which zsh)`



