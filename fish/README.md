# fish shell configuration


## Configuration

very nice configuration and theming of fish can be achieved with the 
[om-my-fish](https://github.com/oh-my-fish/oh-my-fish) framework.

Just install oh-my-fish (omf) with:

	curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install | fish


## Theming

Themes can be installed with:

	omf install <theme-name>

A clean theme is, e.g.: clearance

An informative one: bira


A nice overview of themes can be found on the [omf-themes](https://github.com/oh-my-fish/oh-my-fish/blob/master/docs/Themes.md) page.
