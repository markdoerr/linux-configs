# some useful aliases of benjamin lear
export BLSOURCEDIR="$HOME/source"
export BLDATADIR="$HOME/data"

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'


alias cdpro='cd $BLSOURCEDIR/projects/$1'
alias cdex='cd $BLSOURCEDIR/examples/$1'
alias cdcol='cd $BLSOURCEDIR/collection/$1'
alias cdsrc='cd $BLSOURCEDIR/source/$1'
alias cdpub='cd $BLDATADIR/publications/$1'
alias cdrob='cd $BLDATADIR/robot/robot_data/$1'

alias findfile='find . -type f -name '


# autodoc molecular modelling aliases 
#alias pmv='/opt/mgltools_x86_64Linux2_1.5.6/bin/pmv'
#alias adt='/opt/mgltools_x86_64Linux2_1.5.6/bin/adt'
#alias vision='/opt/mgltools_x86_64Linux2_1.5.6/bin/vision'
#alias pythonsh='/opt/mgltools_x86_64Linux2_1.5.6/bin/pythonsh'

