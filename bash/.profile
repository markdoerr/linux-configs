# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

# RUST cargo
if [ -d "$HOME/.cargo" ] ; then
    source "$HOME/.cargo/env"
fi

# android studio:
if [ -d "$HOME/android-studio/bin" ] ; then
    PATH="$HOME/android-studio/bin:$PATH"
fi


# ROS2
# source /opt/ros/galactic/setup.bash

# java package manager maven
if [ -d "/usr/share/maven/bin" ] ; then
    PATH=/usr/share/maven/bin:$PATH
fi

# molecular modelling Yasara 
if [ -d "/opt/yasara" ] ; then
    PATH="/opt/yasara:$PATH"
fi

# autodock molecular modelling
if [ -d "/opt/mgltools_x86_64Linux2_1.5.6" ] ; then
    PATH=/opt/mgltools_x86_64Linux2_1.5.6/bin:$PATH
fi






