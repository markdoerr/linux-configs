# extending bash with further commands


##export WORKON_HOME=$HOME/.virtualenvs
##export PROJECT_HOME=$HOME/Devel
##source /usr/local/bin/virtualenvwrapper.sh

# bin
# export PATH="$HOME/bin:$PATH"

# dotnet 
# opt-out telemetry
export DOTNET_CLI_TELEMETRY_OPTOUT="true"

#export QT_AUTO_SCREEN_SCALE_FACTOR=1.0
#export QT_SCALE_FACTOR=1.0
#source "$HOME/.cargo/env"
