# some useful functions:

# simpler activation of a python environment
function pyact() {
  source $HOME/py3venv/$1/bin/activate;	
} 

# removing leading and trailing square brackets within a file
# this might be useful for fixing some malformed JSON files
function rm_brackets { head -n -1 $1 | tail -n +2; }

if test -n "$BASH_VERSION"; then
  export -f rm_brackets
fi
